process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');
var path = require('path');
require('laravel-elixir-livereload');

elixir.config.assetsPath = __dirname;
elixir.config.css.less.folder = '';
elixir.config.js.folder = '';
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var BOWER_PATH = 'resources/assets/bower_components';
var RESOURCES_PATH = 'resources';
var ASSETS_PATH = RESOURCES_PATH + '/assets';
var VENDORS_PATH = ASSETS_PATH + '/vendor';

elixir(function(mix) {

	mix
		.copy(path.join(BOWER_PATH, 'bootstrap', 'fonts'), path.join('public','fonts'))
		.copy(path.join(BOWER_PATH, 'font-awesome', 'fonts'), path.join('public','fonts'))
		.copy(path.join(BOWER_PATH, 'owl.carousel', 'src', 'img'), path.join('public','img'))
		.copy(path.join(BOWER_PATH, 'lightbox2', 'dist', 'images'), path.join('public','images'));
});

elixir(function(mix) {
	mix.less(
		ASSETS_PATH + '/less/bootstrap.less',
		ASSETS_PATH + '/tmp'
	).less(
		BOWER_PATH + '/font-awesome/less/font-awesome.less',
		ASSETS_PATH + '/tmp'
	).less(
		ASSETS_PATH + '/less/app.less'
	);
});

elixir(function(mix) {
	mix
		.styles([
			ASSETS_PATH + '/tmp/bootstrap.css',
			ASSETS_PATH + '/tmp/font-awesome.css',
			BOWER_PATH + '/sweetalert2/dist/sweetalert2.min.css',
			BOWER_PATH + '/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
			BOWER_PATH + '/bootstrap-select/dist/css/bootstrap-select.min.css',
			BOWER_PATH + '/owl.carousel/dist/assets/owl.carousel.min.css',
			BOWER_PATH + '/lightbox2/dist/css/lightbox.min.css'
		], 'public/css/vendors.css', __dirname)
		.livereload(null, { host:"devel.geminiano.menu" })
	;
});

elixir(function(mix) {
	mix
		.scripts([
			ASSETS_PATH + '/js/application.js',
			ASSETS_PATH + '/js/common.js',
			ASSETS_PATH + '/js/launcher.js',
		], 'public/js/app.js')
		.scripts([
			BOWER_PATH + '/jquery/dist/jquery.min.js',
			BOWER_PATH + '/bootstrap/dist/js/bootstrap.js',
			BOWER_PATH + '/sweetalert2/dist/sweetalert2.min.js',
			BOWER_PATH + '/bootstrap-switch/dist/js/bootstrap-switch.min.js',
			BOWER_PATH + '/bootstrap-select/dist/js/bootstrap-select.min.js',
			BOWER_PATH + '/jquery-file-upload/js/vendor/jquery.ui.widget.js',
			BOWER_PATH + '/jquery-file-upload/js/jquery.iframe-transport.js',
			BOWER_PATH + '/jquery-file-upload/js/jquery.fileupload.js',
			BOWER_PATH + '/owl.carousel/dist/owl.carousel.min.js',
			BOWER_PATH + '/lightbox2/dist/js/lightbox.min.js',
		], 'public/js/vendors.js').livereload(null, { host:"devel.task.force" });
});