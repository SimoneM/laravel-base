@if($collection && $collection->count() > 0)
	<div class="row">
		<div class="col-md-6">
			<p style="margin: 27px 0">
				Mostra da {!! ($collection->currentPage() - 1) * $collection->perPage() + 1 !!} a {!! $collection->currentPage() != $collection->lastPage() ? $collection->currentPage() * $collection->perPage() : $collection->total() !!} di {!! $collection->total() !!} righe
			</p>
		</div>
		<div class="col-md-6 text-right">
			{!! $collection->appends(request()->except(['_token']))->render() !!}
		</div>
	</div>
@endif
