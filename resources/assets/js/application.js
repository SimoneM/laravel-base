Application = {

    /**
     * show or hide console messages
     */
    DEBUG: true,

    /**
     * Debug function
     * @param data
     * @param debug
     */
    debug: function(data, debug) {
        if(debug === undefined && Application.DEBUG) {
            console.log(data);
        }
    }
};