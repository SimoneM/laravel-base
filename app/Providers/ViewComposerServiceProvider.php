<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->composeMaster();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 *
	 * inject controller and action name in body tag
	 *
	 */
	public function composeMaster()
	{
		view()->composer('layouts.app', function($view)
		{
			$action = app('request')->route()->getAction();
			$controller = class_basename($action['controller']);
			$show_sidebar = true;
			list($controller, $action) = explode('@', $controller);
			/*
			 * remove Controller word in controller name
			 */
			$controller = substr_replace($controller, '', strpos($controller, 'Controller'));
			$view->with(compact('controller', 'action', 'show_sidebar'));
		});
	}
}
