<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	public function up()
	{
		Schema::create('users', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('province_id')->unsigned();

			$table->string('name', 25);
			$table->string('surname', 25);
			$table->string('school', 25);
			$table->string('gender', 1);
			$table->string('address');
			$table->string('cap');
			$table->string('phone_number');
			$table->string('email')->unique();
			$table->string('password');

			$table->boolean('enabled')->default(true);
			$table->boolean('privacy')->default(false);

			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::table('users', function (Blueprint $table)
		{
			$table->foreign('province_id')
				->references('id')->on('cities');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}
